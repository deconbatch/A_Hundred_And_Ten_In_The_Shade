/**
 * A Hundred And Ten In The Shade.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * created 2019.08.15
 * an animation of Vector Field with perlin noise in angle.
 */

void setup() {

  size(720, 720);
  colorMode(HSB, 360, 100, 100, 100);
  smooth();
  noLoop();

}

void draw() {

  int   frameMax  = 14 * 4;
  float radiusMax = 0.35;
  float baseHue   = random(360.0);
  float initDiv   = radiusMax / (frameMax + 1);

  background(baseHue, 0.0, 90.0, 100);

  
  for (float radius = initDiv; radius <= radiusMax; radius += initDiv) {

    float radiusRatio = map(radius, initDiv, radiusMax, 0.0, 1.0);

    blendMode(DIFFERENCE);
    noStroke();
    drawDots(baseHue, radius * 50.0, radius, radiusRatio);
    blendMode(BLEND);
    casing(baseHue);
    saveFrame("frames/01" + String.format("%04d", floor(radius * 1000)) + ".png");
  }

  // for completed still images
  for (int i = 0; i < 14 * 2; i++) {
    saveFrame("frames/02" + String.format("%04d", i) + ".png");
  }

  // for twitter thumnail
  saveFrame("frames/00.00000.png");
  exit();

}

/**
 * drawDots
 * @param  _baseHue : vector field color.
 * @param  _nMult   : noise multiple value.
 * @param  _radius  : distance from center of canvas to plot point.
 * @param  _density : density ratio of dots.
 */
private void drawDots(float _baseHue, float _nMult, float _radius, float _density) {

  // shape
  float plotDiv = 0.001;
  float baseSiz = 0.5;

  // color
  float baseSat = 90.0;
  float baseBri = 20.0;
  float baseAlp = 50.0;

    float radianDiv = map(_density, 0.0, 1.0, 0.1, 0.025);
    int   plotMax   = floor(map(_density, 0.0, 1.0, 100.0, 7500.0));

    for (float radian = 0.0; radian < TWO_PI; radian += radianDiv) {

      // initial points makes circle shape
      float xInit  = _radius * cos(radian + _radius);
      float yInit  = _radius * sin(radian + _radius);
      int   sparse = floor((xInit + yInit) * 100.0) % 4;  // make 0, 1, 2, 3 with initial point

      // draw vector field
      float rPoint = 0.0;
      float xPoint = xInit;
      float yPoint = yInit;
      for (int plotCnt = 0; plotCnt < plotMax; ++plotCnt) {

        float pRatio = map(plotCnt, 0, plotMax, 0.0, 1.0);

        // vector field calculation
        float rPrev = rPoint;
        float xPrev = xPoint;
        float yPrev = yPoint;
        rPoint += sin((noise(xPrev * 10.0) + noise(yPrev * 10.0)) * _nMult);
        xPoint += plotDiv * cos(TWO_PI * rPoint);
        yPoint += plotDiv * sin(TWO_PI * rPoint);

        float pHue = _baseHue + pRatio * 120.0;
        float pSat = baseSat * map(sin(PI * pRatio), 0.0, 1.0, 1.0, 0.5);
        float pBri = baseBri * sin(PI * pRatio) * (sparse + 6.0) / 9.0;
        float pSiz = baseSiz * sin(PI * pRatio);
        fill(pHue % 360.0, pSat, pBri, baseAlp);
        ellipse((xPoint + 0.5) * width, (yPoint + 0.5) * height, pSiz, pSiz);

      }
    }

}

/**
 * casing : draw fancy casing
 * @param  _baseHue : casing color.
 */
private void casing(float _baseHue) {
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(60.0);
  stroke((_baseHue + 240.0) % 360.0, 90.0, 10.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
}
